﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace PrestoSpesa
{
	public class ListaAdapterThree: BaseAdapter
	{

		Color CR1 = Color.Rgb(27, 160, 153);
		Color CR2 = Color.Rgb(150, 194, 46);
		Color CR3 = Color.Rgb(57, 174, 107);

		Activity context;

		public List<ArticoloCarrello> items;

		int indice;

		public ListaAdapterThree(Activity context, int i) : base()
		{
			this.context = context;
			indice = i;
			//For demo purposes we hard code some data here
			this.items = new List<ArticoloCarrello>()
			{
			};
		}

		public override int Count
		{
			get { return items.Count; }
		}

		public override Java.Lang.Object GetItem(int position)
		{
			return position;
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			//Get our object for this position
			//var item = items[position];  

			View view = convertView;

			Typeface osr = Typeface.CreateFromAsset(context.Assets, "fonts/OpenSansRegular.ttf");

			ArticoloCarrello dItem = this.items[position];

			LayoutInflater inflater = ((Activity)context).LayoutInflater;

			view = inflater.Inflate(Resource.Layout.ArticoloCarrelloLayout, parent, false);

			view.FindViewById<ImageView>(Resource.Id.imageView1).Click += delegate
			{
				dItem.qnt++;

				var a = context as HomePageThree;
				a.UpdateField(indice, dItem.prezzo, true);

				this.NotifyDataSetChanged();
			};
			view.FindViewById<ImageView>(Resource.Id.imageView2).Click += delegate
			{
				dItem.qnt--;

				var a = context as HomePageThree;

				if (dItem.qnt == 0)
				{
					a.CheckRemove(dItem, indice);
				}
				else {
					a.UpdateField(indice, dItem.prezzo, false);
				}

				this.NotifyDataSetChanged();
			};

			if (indice == 1)
				view.FindViewById<LinearLayout>(Resource.Id.linearLayout1).SetBackgroundColor(CR1);
			if (indice == 2)
				view.FindViewById<LinearLayout>(Resource.Id.linearLayout1).SetBackgroundColor(CR2);
			if (indice == 3)
				view.FindViewById<LinearLayout>(Resource.Id.linearLayout1).SetBackgroundColor(CR3);

			view.FindViewById<TextView>(Resource.Id.textView1).Text = dItem.qnt.ToString();
			view.FindViewById<TextView>(Resource.Id.textView2).Text = dItem.titolo;
			view.FindViewById<TextView>(Resource.Id.textView3).Text = dItem.prezzo.ToString("F") + " €";

			view.FindViewById<TextView>(Resource.Id.textView1).Typeface = osr;
			view.FindViewById<TextView>(Resource.Id.textView2).Typeface = osr;
			view.FindViewById<TextView>(Resource.Id.textView3).Typeface = osr;

			return view;


			//return view;
		}

		public ArticoloCarrello GetItemAtPosition(int position)
		{
			return items[position];
		}


		public void RemoveArticle(ArticoloCarrello obj, bool elimina)
		{

			if (elimina)
			{
				items.Remove(obj);

			}
			else {
				obj.qnt++;
			}
			this.NotifyDataSetChanged();

		}
	}
}



