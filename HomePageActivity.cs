﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Graphics;

namespace PrestoSpesa
{
	[Activity(Label = "HomePageActivity",ScreenOrientation = ScreenOrientation.Portrait)]
	public class HomePageActivity : Activity
	{

		int i = 0;

		Color CR1 = Color.Rgb(27,160,153);
		Color CR2 = Color.Rgb(150,194,46);
		Color CR3 = Color.Rgb(57,174,107);


		RelativeLayout BtnCarrello1, BtnCarrello2, BtnCarrello3;

		View Riga1, Riga2, Riga3;

		RelativeLayout Container1, Container2, Container3;

		bool isOne = true;
		bool isSecond = false;
		bool isThird = false;

		ListView List1, List2, List3;
		ListAdapter LA1, LA2, LA3;

		TextView TotColli1, TotColli2, TotColli3;
		TextView TotSpesa1, TotSpesa2, TotSpesa3;

		RelativeLayout BtnScanFoto1, BtnScanFoto2, BtnScanFoto3;
		RelativeLayout BtnScanMan1, BtnScanMan2, BtnScanMan3;
		RelativeLayout BtnFine1, BtnFine2, BtnFine3;


		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.HomePageLayout);
			ActionBar.Hide();

			BtnCarrello1 = FindViewById<RelativeLayout>(Resource.Id.Carrello1);
			BtnCarrello2 = FindViewById<RelativeLayout>(Resource.Id.Carrello2);
			BtnCarrello3 = FindViewById<RelativeLayout>(Resource.Id.Carrello3);

			Riga1 = FindViewById<View>(Resource.Id.view1);
			Riga2 = FindViewById<View>(Resource.Id.view2);
			Riga3 = FindViewById<View>(Resource.Id.view3);

			Container1 = FindViewById<RelativeLayout>(Resource.Id.Container1);
			Container2 = FindViewById<RelativeLayout>(Resource.Id.Container2);
			Container3 = FindViewById<RelativeLayout>(Resource.Id.Container3);

			Container1.Visibility = ViewStates.Visible;
			Container2.Visibility = ViewStates.Gone;
			Container3.Visibility = ViewStates.Gone;

			List1 = FindViewById<ListView>(Resource.Id.list1);
			List2 = FindViewById<ListView>(Resource.Id.list2);
			List3 = FindViewById<ListView>(Resource.Id.list3);

			LA1 = new ListAdapter(this,1);
			LA2 = new ListAdapter(this,2);
			LA3 = new ListAdapter(this,3);

			List1.SetAdapter(LA1);
			List1.SetFooterDividersEnabled(false);
			List1.SetHeaderDividersEnabled(false);
			List1.Divider.SetAlpha(0);
			List1.ContextClickable = true;
			List1.Clickable = false;
			List1.ItemClick += delegate
			{
			};

			List2.SetAdapter(LA2);
			List2.SetFooterDividersEnabled(false);
			List2.SetHeaderDividersEnabled(false);
			List2.Divider.SetAlpha(0);
			List2.ContextClickable = true;
			List2.Clickable = false;
			List2.ItemClick += delegate
			{
			};

			List3.SetAdapter(LA3);
			List3.SetFooterDividersEnabled(false);
			List3.SetHeaderDividersEnabled(false);
			List3.Divider.SetAlpha(0);
			List3.ContextClickable = true;
			List3.Clickable = false;
			List3.ItemClick += delegate
			{
			};

			TotColli1 = FindViewById<TextView>(Resource.Id.totColli1);
			TotColli2 = FindViewById<TextView>(Resource.Id.totColli2);
			TotColli3 = FindViewById<TextView>(Resource.Id.totColli3);

			TotColli1.Text = "Tot. colli 0";
			TotColli2.Text = "Tot. colli 0";
			TotColli3.Text = "Tot. colli 0";

			TotSpesa1 = FindViewById<TextView>(Resource.Id.totSpesa1);
			TotSpesa2 = FindViewById<TextView>(Resource.Id.totSpesa2);
			TotSpesa3 = FindViewById<TextView>(Resource.Id.totSpesa3);

			TotSpesa1.Text = "Tot. spesa 0€";
			TotSpesa2.Text = "Tot. spesa 0€";
			TotSpesa3.Text = "Tot. spesa 0€";

			BtnScanFoto1 = FindViewById<RelativeLayout>(Resource.Id.BtnScanFoto1);
			BtnScanFoto2 = FindViewById<RelativeLayout>(Resource.Id.BtnScanFoto2);
			BtnScanFoto3 = FindViewById<RelativeLayout>(Resource.Id.BtnScanFoto3);

			BtnScanMan1 = FindViewById<RelativeLayout>(Resource.Id.BtnScanMan1);
			BtnScanMan2 = FindViewById<RelativeLayout>(Resource.Id.BtnScanMan2);
			BtnScanMan3 = FindViewById<RelativeLayout>(Resource.Id.BtnScanMan3);

			BtnFine1 = FindViewById<RelativeLayout>(Resource.Id.BtnFine1);
			BtnFine2 = FindViewById<RelativeLayout>(Resource.Id.BtnFine2);
			BtnFine3 = FindViewById<RelativeLayout>(Resource.Id.BtnFine3);


			BtnCarrello1.Click += delegate {

				if (!isOne) {

					Riga1.SetBackgroundColor(CR1);
					Riga2.SetBackgroundColor(Color.White);
					Riga3.SetBackgroundColor(Color.White);

					Container1.Visibility = ViewStates.Visible;
					Container2.Visibility = ViewStates.Gone;
					Container3.Visibility = ViewStates.Gone;

					isOne = true;
					isSecond = false;
					isThird = false;

				}

			};

			BtnCarrello2.Click += delegate
			{

				if (!isSecond)
				{

					Riga1.SetBackgroundColor(Color.White);
					Riga2.SetBackgroundColor(CR2);
					Riga3.SetBackgroundColor(Color.White);

					Container1.Visibility = ViewStates.Gone;
					Container2.Visibility = ViewStates.Visible;
					Container3.Visibility = ViewStates.Gone;

					isOne = false;
					isSecond = true;
					isThird = false;

				}

			};

			BtnCarrello3.Click += delegate
			{

				if (!isThird)
				{

					Riga1.SetBackgroundColor(Color.White);
					Riga2.SetBackgroundColor(Color.White);
					Riga3.SetBackgroundColor(CR3);

					Container1.Visibility = ViewStates.Gone;
					Container2.Visibility = ViewStates.Gone;
					Container3.Visibility = ViewStates.Visible;

					isOne = false;
					isSecond = false;
					isThird = true;

				}

			};


/*
			Button b1 = FindViewById<Button>(Resource.Id.button1);
			b1.Click+= delegate {

				var random = new System.Random();

				float value = (float)((float)random.Next(1, 1000) / (float)1000);

				Console.WriteLine(random.Next(1, 10) + "|" + value);

				ArticoloCarrello ac = new ArticoloCarrello();
				ac.id = "aaaadfeg";
				ac.prezzo = (random.Next(1,10) + value);
				ac.qnt = 1;
				ac.titolo = "Articolo " + i;

				LA.items.Add(ac);

				LA.NotifyDataSetChanged();

			};*/
		}
	}
}

