﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Graphics;
using Android.Views.InputMethods;
using ZXing.Mobile;
using System.Timers;
using RestSharp;
using System.Net;
using System.Collections.Specialized;
using Android.Media;
using System.Threading;

namespace PrestoSpesa
{
	[Activity(Label = "HomePageOne", ScreenOrientation = ScreenOrientation.Portrait)]
	public class HomePageOne : Activity
	{
		public static string TXTCOLLI = "Tot. colli";
		public static string TXTSPESA = "Tot. spesa";

		int i = 0;

		Color CR1 = Color.Rgb(27, 160, 153);

		public Dictionary<string, ArticoloObject> ArticoloDictionary;

		RelativeLayout BtnCarrello1, Load;

		View Riga1;

		RelativeLayout Container1;

		RelativeLayout ViewFine1;

		bool isOne = true;
		bool isSecond = false;

		ListView List1;
		ListAdapterOne LA1;

		TextView TotColli1;
		TextView TotSpesa1;

		RelativeLayout BtnScanFoto1;
		RelativeLayout BtnScanMan1;
		RelativeLayout BtnFine1;

		public int colli1;
		public float spesa1;

		MobileBarcodeScanner scanner;

		bool scanOpen = false;
		bool scanFineOpen = false;

		public System.Timers.Timer timer;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.HomePageOneLayout);
			ActionBar.Hide();

			ArticoloDictionary = MainActivity.Instance.ArticoloDictionary;

			BtnCarrello1 = FindViewById<RelativeLayout>(Resource.Id.Carrello1);

			Load = FindViewById<RelativeLayout>(Resource.Id.Download);
			Load.Visibility = ViewStates.Gone;

			Riga1 = FindViewById<View>(Resource.Id.view1);

			Container1 = FindViewById<RelativeLayout>(Resource.Id.Container1);

			Container1.Visibility = ViewStates.Visible;

			ViewFine1 = FindViewById<RelativeLayout>(Resource.Id.ViewChiuso1);

			ViewFine1.Visibility = ViewStates.Gone;

			List1 = FindViewById<ListView>(Resource.Id.list1);

			LA1 = new ListAdapterOne(this, 1);

			List1.SetAdapter(LA1);
			List1.SetFooterDividersEnabled(false);
			List1.SetHeaderDividersEnabled(false);
			List1.Divider.SetAlpha(0);
			//Console.WriteLine(Android.OS.Build.VERSION.SdkInt.ToString());
			//if(Android.OS.Build.VERSION.SdkInt > BuildVersionCodes.LollipopMr1)
			//List1.ContextClickable = true;
			List1.Clickable = false;
			List1.ItemClick += delegate
			{
			};

			TotColli1 = FindViewById<TextView>(Resource.Id.totColli1);

			TotColli1.Text = TXTCOLLI + " " + colli1;

			TotSpesa1 = FindViewById<TextView>(Resource.Id.totSpesa1);

			TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";

			BtnScanFoto1 = FindViewById<RelativeLayout>(Resource.Id.BtnScanFoto1);

			BtnScanMan1 = FindViewById<RelativeLayout>(Resource.Id.BtnScanMan1);

			BtnFine1 = FindViewById<RelativeLayout>(Resource.Id.BtnFine1);

			FindViewById<ProgressBar>(Resource.Id.progressBar1).IndeterminateDrawable.SetColorFilter(Color.White, PorterDuff.Mode.Multiply);

			BtnScanFoto1.Click += async delegate
			{

				var options = new ZXing.Mobile.MobileBarcodeScanningOptions();
				options.PossibleFormats = new List<ZXing.BarcodeFormat>() {
					ZXing.BarcodeFormat.CODE_128,
					ZXing.BarcodeFormat.EAN_13
				};
				options.AutoRotate = false;
				options.TryHarder = true;

				scanOpen = true;

				scanner = new ZXing.Mobile.MobileBarcodeScanner();
				var result = await scanner.Scan(options);

				if (result != null)
				{
					Console.WriteLine(result.BarcodeFormat);
					HandleScanResult(result, 1);
				}
				else
					scanOpen = false;
			};

			BtnScanMan1.Click += delegate
			{

				var inputDialog = new AlertDialog.Builder(this);
				EditText userInput = new EditText(this);
				userInput.Hint = "Codice prodotto";
				//SetEditTextStylings(userInput);
				userInput.InputType = Android.Text.InputTypes.TextFlagMultiLine;
				inputDialog.SetTitle("Articolo");
				inputDialog.SetMessage("Inserisci il codice prodotto.");
				inputDialog.SetView(userInput);
				inputDialog.SetCancelable(false);
				inputDialog.SetPositiveButton("Ok", (see, ess) =>
				{
					bool result = AddArticolo(userInput.Text.ToUpper(), 1);
					HideKeyboard(userInput);
					if (!result)
					{
						AlertDialog.Builder alert = new AlertDialog.Builder(this);
						alert.SetTitle("Errore");
						alert.SetMessage("Codice Errato o non presente");
						alert.SetPositiveButton("Continua", (senderAlert, args) =>
						{
						});
						alert.SetCancelable(false);
						alert.Show();
					}
				});

				inputDialog.SetNegativeButton("Annulla", (afk, kfa) =>
				{
					HideKeyboard(userInput);
				});

				inputDialog.Show();

				ShowKeyboard(userInput);

			};

			BtnFine1.Click += async delegate
			{

				var options = new ZXing.Mobile.MobileBarcodeScanningOptions();
				options.PossibleFormats = new List<ZXing.BarcodeFormat>() {
					ZXing.BarcodeFormat.CODE_128
				};
				options.AutoRotate = false;
				options.TryHarder = true;

				scanFineOpen = true;

				scanner = new ZXing.Mobile.MobileBarcodeScanner();

				ZXing.Result result = null;

				new Thread(new ThreadStart(delegate
				{
					while (result == null)
					{
						Console.WriteLine("AF");
						scanner.AutoFocus();
						Thread.Sleep(2000);
					}
				})).Start();

				result = await scanner.Scan(options);

				if (result != null)
					HandleScanResultFine(result, 1);
				else
					scanFineOpen = false;
			};

			BtnCarrello1.Click += delegate
			{

				if (!isOne)
				{

					Riga1.SetBackgroundColor(CR1);

					Container1.Visibility = ViewStates.Visible;

					isOne = true;

				}

			};

			timer = new System.Timers.Timer();
			timer.Interval = 10 * 60 * 1000;
			timer.Elapsed += (sender, e) =>
			{
				if (!scanFineOpen)
				{
					RunOnUiThread(() =>
					{

						if (scanOpen)
							scanner.Cancel();

						Load.Visibility = ViewStates.Visible;

						Console.WriteLine("TIMER");

						startDownload();

					});
				}

			};
			timer.Enabled = true;
			timer.Start();



			//********** SETTiNG FONT ***********

			Typeface osr = Typeface.CreateFromAsset(Assets, "fonts/OpenSansRegular.ttf");
			Typeface osb = Typeface.CreateFromAsset(Assets, "fonts/OpenSansBold.ttf");

			FindViewById<TextView>(Resource.Id.textView1).Typeface = osb;

			TotColli1.Typeface = osr;

			TotSpesa1.Typeface = osr;

			FindViewById<TextView>(Resource.Id.f1).Typeface = osb;

			FindViewById<TextView>(Resource.Id.m1).Typeface = osb;

			FindViewById<TextView>(Resource.Id.c1).Typeface = osb;
			FindViewById<TextView>(Resource.Id.c1).Text = "CONCLUDI\nSPESA";
			                                      
			FindViewById<TextView>(Resource.Id.txt1).Typeface = osb;

			FindViewById<TextView>(Resource.Id.textView7).Typeface = osr;
			FindViewById<TextView>(Resource.Id.textView8).Typeface = osr;



		}


		void HandleScanResult(ZXing.Result result, int Indice)
		{

			scanOpen = false;

			string msg = "";

			if (result != null && !string.IsNullOrEmpty(result.Text))
			{
				string id = result.Text;
				this.RunOnUiThread(() => {

					bool result2 = AddArticolo(id, Indice);
					//HideKeyboard(userInput);
					if (!result2)
					{
						AlertDialog.Builder alert = new AlertDialog.Builder(this);
						alert.SetTitle("Errore");
						alert.SetMessage("Codice Errato o non presente");
						alert.SetPositiveButton("Continua", (senderAlert, args) =>
						{
						});
						alert.SetCancelable(false);
						alert.Show();
					}
					else
					{
						Toast.MakeText(this, "Prodotto inserito correttamente", ToastLength.Short).Show();
						ToneGenerator toneGen1 = new ToneGenerator(Stream.Music, 100);
						toneGen1.StartTone(Tone.CdmaAlertCallGuard, 1000);
					}
				});
			}
			else
			{

				this.RunOnUiThread(() => Toast.MakeText(this, msg, ToastLength.Short).Show());
			}


		}

		public bool AddArticolo(string id, int Indice)
		{

			if (ArticoloDictionary.ContainsKey(id))
			{
				Console.WriteLine("esiste");
				if (Indice == 1)
				{

					bool GiaInserito = false;

					foreach (var a in LA1.items)
					{

						if (a.id == id)
						{
							GiaInserito = true;
							a.qnt++;
						}
					}

					ArticoloObject ao = ArticoloDictionary[id];

					if (!GiaInserito)
					{

						LA1.items.Add(new ArticoloCarrello(ao.id, 1, ao.prezzo, ao.titolo));

					}

					LA1.NotifyDataSetChanged();

					spesa1 += ao.prezzo;
					colli1++;

					TotColli1.Text = TXTCOLLI + " " + colli1;
					TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";

				}

				return true;

			}
			else {
				return false;
			}

		}

		public void UpdateField(int indice, float prezzo, bool add)
		{

			if (indice == 1)
			{
				if (add)
				{
					spesa1 += prezzo;
					colli1++;
				}
				else {
					spesa1 -= prezzo;
					colli1--;
				}

				TotColli1.Text = TXTCOLLI + " " + colli1;
				TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";
			}

		}

		public void CheckRemove(ArticoloCarrello obj, int i)
		{


			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.SetTitle("Rimozione");
			alert.SetMessage("Stai rimuovendo " + obj.titolo);
			alert.SetPositiveButton("Continua", (senderAlert, args) =>
			{
				if (i == 1)
				{
					LA1.RemoveArticle(obj, true);
					UpdateField(i, obj.prezzo, false);
				}


			});

			alert.SetNegativeButton("Annulla", (senderAlert, args) =>
			{
				if (i == 1)
					LA1.RemoveArticle(obj, false);

			});

			alert.SetCancelable(false);

			alert.Show();

		}

		public void ShowKeyboard(EditText userInput)
		{
			userInput.RequestFocus();
			InputMethodManager imm = (InputMethodManager)this.GetSystemService(Context.InputMethodService);
			imm.ToggleSoftInput(ShowFlags.Forced, 0);
		}

		public void HideKeyboard(EditText userInput)
		{
			InputMethodManager imm = (InputMethodManager)this.GetSystemService(Context.InputMethodService);
			imm.HideSoftInputFromWindow(userInput.WindowToken, 0);
		}

		public void startDownload()
		{
			/* v1 
			//PRODUZIONE
			var client = new RestClient("http://2.228.89.216");
			//DEVELOP
			//var client = new RestClient("http://test.netwintec.com");

			var request = new RestRequest("/prestospesa-backend/read.php", Method.GET);

			request.AddHeader("access-token", "SCOj4qU5JB4ysjUKG1lDgR04hID4WU");

			client.ExecuteAsync(request, (s, e) =>
			{


				//Console.WriteLine(s.StatusCode + "\nContent:\n" + s.Content);

				if (s.StatusCode == HttpStatusCode.OK)
				{

					RunOnUiThread(() =>
					{

						string csv = s.Content;

						//Console.WriteLine(1);

						string[] Lines = csv.Split('\n');

						//Console.WriteLine("2|" + Lines.Length);

						for (int i = 0; i < Lines.Length - 1; i++)
						{
							try
							{
								string lines = Lines[i];
								//Console.WriteLine(lines);
								string[] Value = lines.Split(';');

								//Console.WriteLine(Value[0] + "|" + Value[1] + "|" + Value[2]);

								string Id = Value[0];
								string Tit = Value[1];
								string PrApp = Value[2];

								float Pr = float.Parse(PrApp);//PrApp.Replace(',', '.'));

								if (Id != ".")
								{
									if (ArticoloDictionary.ContainsKey(Id))
										ArticoloDictionary[Id] = new ArticoloObject(Id, Pr, Tit);
									else
										ArticoloDictionary.Add(Id, new ArticoloObject(Id, Pr, Tit));
								}

							}
							catch (Exception ee)
							{

							}
						}

						//Console.WriteLine(3);

						spesa1 = 0;

						foreach (var a in LA1.items)
						{
							a.prezzo = ArticoloDictionary[a.id].prezzo;

							spesa1 += (a.prezzo * a.qnt);

						}


						TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";

						LA1.NotifyDataSetChanged();

						Load.Visibility = ViewStates.Gone;

					});
				}
				else
				{
				}

			});

*/

			/** V2 **/

			MainActivity.Instance.timeoutPage = true;

			try
			{

				var webclient = new MyWebClientPage();

				webclient.DownloadDataCompleted += (s, e) =>
				{

					RunOnUiThread(() =>
					{
						MainActivity.Instance.timeoutPage = false;
					});

					if (e.Cancelled)
					{
						Console.WriteLine("CANCELLED");
					}
					if (e.Error != null)
					{
						Console.WriteLine("ERROR" + e.Error.Message);

						if (e.Error.Message.Contains("401"))
						{
							RunOnUiThread(() =>
							{
								spesa1 = 0;
								colli1 = 0;
								LA1.items.Clear();

								TotColli1.Text = TXTCOLLI + " " + colli1;
								TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";

								LA1.NotifyDataSetChanged();

								Load.Visibility = ViewStates.Gone;
								//Errore(1);
							});
						}
						else {
							RunOnUiThread(() =>
							{
								Load.Visibility = ViewStates.Gone;
								//Errore(2);
							});
						}

					}
					if (e.Error == null && !e.Cancelled)
					{
						RunOnUiThread(() =>
						{
							var bytes = e.Result;
							string result = System.Text.Encoding.UTF8.GetString(bytes);
							//Console.WriteLine("Result" + result);

							string csv = result;

							Console.WriteLine(1);

							string[] Lines = csv.Split('\n');

							//Console.WriteLine("2|" + Lines.Length);

							for (int i = 0; i < Lines.Length - 1; i++)
							{
								try
								{
									string lines = Lines[i];
									//Console.WriteLine(lines);
									string[] Value = lines.Split(';');

									//Console.WriteLine(Value[0] + "|" + Value[1] + "|" + Value[2]);

									string Id = Value[0];
									string Tit = Value[1];
									string PrApp = Value[2];

									float Pr = float.Parse(PrApp);//PrApp.Replace(',', '.'));

									if (Id != ".")
									{
										if (ArticoloDictionary.ContainsKey(Id))
											ArticoloDictionary[Id] = new ArticoloObject(Id, Pr, Tit);
										else
											ArticoloDictionary.Add(Id, new ArticoloObject(Id, Pr, Tit));
									}

								}
								catch (Exception ee)
								{

								}
							}

							//Console.WriteLine(3);

							spesa1 = 0;

							foreach (var a in LA1.items)
							{
								a.prezzo = ArticoloDictionary[a.id].prezzo;

								spesa1 += (a.prezzo * a.qnt);

							}


							TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";

							LA1.NotifyDataSetChanged();

							Load.Visibility = ViewStates.Gone;

						});

					}
				};

				// DEVELOPMENT
				//var url = new System.Uri("http://test.netwintec.com/prestospesa-backend/read.php");
				//PRODUZIONE
				var url = new System.Uri("http://2.228.89.216/prestospesa-backend/read.php");


				var header = new WebHeaderCollection();
				header.Add("access-token", "SCOj4qU5JB4ysjUKG1lDgR04hID4WU");
				webclient.Headers = header;


				webclient.DownloadDataAsync(url);

			}
			catch (Exception ee) { }

		}


		void HandleScanResultFine(ZXing.Result result, int Indice)
		{

			scanOpen = false;

			string msg = "";

			if (result != null && !string.IsNullOrEmpty(result.Text))
			{
				string txt = result.Text;
				this.RunOnUiThread(() => {

					Toast.MakeText(this,"Operazione in corso\nAttendere...", ToastLength.Short).Show();
					ChiudiCarrello(txt, Indice);
				});
			}
			else
			{

				this.RunOnUiThread(() => Toast.MakeText(this, msg, ToastLength.Short).Show());
			}


		}


		public void ChiudiCarrello(string txt, int indice)
		{

			string carrello = "";

			if (indice == 1)
			{
				foreach (var a in LA1.items)
				{

					if (a.id.Length == 12)
						carrello += a.id + " ";
					else
						carrello += a.id;

					if (a.qnt > 0 && a.qnt < 10)
						carrello += "000" + a.qnt;

					if (a.qnt >= 10 && a.qnt < 100)
						carrello += "00" + a.qnt;

					if (a.qnt >= 100 && a.qnt < 1000)
						carrello += "0" + a.qnt;

					if (a.qnt >= 1000 && a.qnt < 10000)
						carrello += a.qnt;

					carrello += "\r\n";
				}
			}

			/* v1
			//PRODUZIONE
			var client = new RestClient("http://2.228.89.216");
			//DEVELOP
			//var client = new RestClient("http://test.netwintec.com");

			var request = new RestRequest("/prestospesa-backend/write.php", Method.POST);

			request.AddHeader("access-token", "SCOj4qU5JB4ysjUKG1lDgR04hID4WU");

			request.AddParameter("codice_cassa", txt);
			request.AddParameter("spesa", carrello);

			client.ExecuteAsync(request, (s, e) =>
			{


				Console.WriteLine("CHIUSURA:" + s.StatusCode + "\nContent:\n" + s.Content);

				if (s.StatusCode == HttpStatusCode.OK)
				{

					RunOnUiThread(() =>
					{

						if (indice == 1)
						{
							ViewFine1.Visibility = ViewStates.Visible;
						}

					});
				}
				else
				{
					RunOnUiThread(() =>
					{

						AlertDialog.Builder alert = new AlertDialog.Builder(this);
						alert.SetTitle("Errore");
						alert.SetMessage("Errore durante l'operazione di chiusura riprovare.");
						alert.SetPositiveButton("OK", (senderAlert, args) =>
						{
						});
						alert.SetCancelable(false);
						alert.Show();

					});
				}

			});

*/

			// v2 

			MainActivity.Instance.timeoutChiusura = true;

			try
			{

				var webclient = new MyWebClientChiusura();

				webclient.UploadValuesCompleted += (s, e) =>
				{

					RunOnUiThread(() =>
					{
						MainActivity.Instance.timeoutChiusura = false;
					});

					if (e.Cancelled)
					{
						Console.WriteLine("CANCELLED");
					}
					if (e.Error != null)
					{
						Console.WriteLine("ERROR" + e.Error.Message);

						if (e.Error.Message.Contains("401"))
						{
							RunOnUiThread(() =>
							{
								AlertDialog.Builder alert = new AlertDialog.Builder(this);
								alert.SetTitle("Errore");
								alert.SetMessage("Errore durante l'operazione di chiusura riprovare.");
								alert.SetPositiveButton("OK", (senderAlert, args) =>
								{
								});
								alert.SetCancelable(false);
								alert.Show();
							});
						}
						else
						{
							RunOnUiThread(() =>
							{
								AlertDialog.Builder alert = new AlertDialog.Builder(this);
								alert.SetTitle("Errore");
								alert.SetMessage("Errore durante l'operazione di chiusura riprovare.");
								alert.SetPositiveButton("OK", (senderAlert, args) =>
								{
								});
								alert.SetCancelable(false);
								alert.Show();
							});
						}

					}
					if (e.Error == null && !e.Cancelled)
					{
						RunOnUiThread(() =>
						{
							if (indice == 1)
							{
								ViewFine1.Visibility = ViewStates.Visible;
							}


						});

					}
				};

				// DEVELOPMENT
				//var url = new System.Uri("http://test.netwintec.com/prestospesa-backend/write.php");
				//PRODUZIONE
				var url = new System.Uri("http://2.228.89.216/prestospesa-backend/write.php");

				var header = new WebHeaderCollection();
				header.Add("access-token", "SCOj4qU5JB4ysjUKG1lDgR04hID4WU");
				webclient.Headers = header;

				//request.AddParameter("codice_cassa", txt);
				//request.AddParameter("spesa", carrello);

				//string dataString = @"{'codice_cassa':'" + txt + "','spesa':'" + carrello + "'}";
				//byte[] dataBytes = Encoding.UTF8.GetBytes(dataString);

				NameValueCollection parameter = new NameValueCollection();
				parameter.Add("codice_cassa", txt);
				parameter.Add("spesa", carrello);

				webclient.UploadValuesAsync(url, "POST", parameter);

			}
			catch (Exception ee) { }
		}

		public override void OnBackPressed()
		{
			//base.OnBackPressed();

			Exit();

		}

		public void Exit()
		{
			//Alert vuoi sloggarti?
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.SetCancelable(false);
			alert.SetTitle("Sicuro di voler chiudere l'applicazione?");
			alert.SetPositiveButton("Si", (senderAlert, args) =>
			{

				//System.Environment.Exit(0);
				Android.OS.Process.KillProcess(Android.OS.Process.MyPid());

			});
			alert.SetNegativeButton("Annulla", (senderAlert, args) =>
			{
				//volendo fa qualcosa
			});
			//fa partire l'alert su di un trhead
			RunOnUiThread(() =>
			{
				alert.Show();
			});
		}

	}
}





