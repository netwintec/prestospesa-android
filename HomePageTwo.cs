﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Graphics;
using Android.Views.InputMethods;
using ZXing.Mobile;
using System.Timers;
using RestSharp;
using System.Net;
using System.Collections.Specialized;
using Android.Media;

namespace PrestoSpesa
{
	[Activity(Label = "HomePageTwo", ScreenOrientation = ScreenOrientation.Portrait)]
	public class HomePageTwo : Activity
	{
		public static string TXTCOLLI = "Tot. colli";
		public static string TXTSPESA = "Tot. spesa";

		int i = 0;

		Color CR1 = Color.Rgb(27, 160, 153);
		Color CR2 = Color.Rgb(150, 194, 46);

		public Dictionary<string, ArticoloObject> ArticoloDictionary;

		RelativeLayout BtnCarrello1, BtnCarrello2, Load;

		View Riga1, Riga2;

		RelativeLayout Container1, Container2;

		RelativeLayout ViewFine1, ViewFine2;

		bool isOne = true;
		bool isSecond = false;

		ListView List1, List2;
		ListAdapterTwo LA1, LA2;

		TextView TotColli1, TotColli2;
		TextView TotSpesa1, TotSpesa2;

		RelativeLayout BtnScanFoto1, BtnScanFoto2;
		RelativeLayout BtnScanMan1, BtnScanMan2;
		RelativeLayout BtnFine1, BtnFine2;

		public int colli1, colli2;
		public float spesa1, spesa2;

		MobileBarcodeScanner scanner;

		bool scanOpen = false;
		bool scanFineOpen = false;

		public Timer timer;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.HomePageTwoLayout);
			ActionBar.Hide();

			ArticoloDictionary = MainActivity.Instance.ArticoloDictionary;

			BtnCarrello1 = FindViewById<RelativeLayout>(Resource.Id.Carrello1);
			BtnCarrello2 = FindViewById<RelativeLayout>(Resource.Id.Carrello2);

			Load = FindViewById<RelativeLayout>(Resource.Id.Download);
			Load.Visibility = ViewStates.Gone;

			Riga1 = FindViewById<View>(Resource.Id.view1);
			Riga2 = FindViewById<View>(Resource.Id.view2);

			Container1 = FindViewById<RelativeLayout>(Resource.Id.Container1);
			Container2 = FindViewById<RelativeLayout>(Resource.Id.Container2);

			Container1.Visibility = ViewStates.Visible;
			Container2.Visibility = ViewStates.Gone;

			ViewFine1 = FindViewById<RelativeLayout>(Resource.Id.ViewChiuso1);
			ViewFine2 = FindViewById<RelativeLayout>(Resource.Id.ViewChiuso2);

			ViewFine1.Visibility = ViewStates.Gone;
			ViewFine2.Visibility = ViewStates.Gone;

			List1 = FindViewById<ListView>(Resource.Id.list1);
			List2 = FindViewById<ListView>(Resource.Id.list2);

			LA1 = new ListAdapterTwo(this, 1);
			LA2 = new ListAdapterTwo(this, 2);

			List1.SetAdapter(LA1);
			List1.SetFooterDividersEnabled(false);
			List1.SetHeaderDividersEnabled(false);
			List1.Divider.SetAlpha(0);
			//Console.WriteLine(Android.OS.Build.VERSION.SdkInt.ToString());
			//if(Android.OS.Build.VERSION.SdkInt > BuildVersionCodes.LollipopMr1)
			//List1.ContextClickable = true;
			List1.Clickable = false;
			List1.ItemClick += delegate
			{
			};

			List2.SetAdapter(LA2);
			List2.SetFooterDividersEnabled(false);
			List2.SetHeaderDividersEnabled(false);
			List2.Divider.SetAlpha(0);
			//Console.WriteLine(Android.OS.Build.VERSION.SdkInt.ToString());
			//if(Android.OS.Build.VERSION.SdkInt > BuildVersionCodes.LollipopMr1)
			//List2.ContextClickable = true;
			List2.Clickable = false;
			List2.ItemClick += delegate
			{
			};

			TotColli1 = FindViewById<TextView>(Resource.Id.totColli1);
			TotColli2 = FindViewById<TextView>(Resource.Id.totColli2);

			TotColli1.Text = TXTCOLLI + " " + colli1;
			TotColli2.Text = TXTCOLLI + " " + colli2;

			TotSpesa1 = FindViewById<TextView>(Resource.Id.totSpesa1);
			TotSpesa2 = FindViewById<TextView>(Resource.Id.totSpesa2);

			TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";
			TotSpesa2.Text = TXTSPESA + " " + spesa2.ToString("F") + "€";

			BtnScanFoto1 = FindViewById<RelativeLayout>(Resource.Id.BtnScanFoto1);
			BtnScanFoto2 = FindViewById<RelativeLayout>(Resource.Id.BtnScanFoto2);

			BtnScanMan1 = FindViewById<RelativeLayout>(Resource.Id.BtnScanMan1);
			BtnScanMan2 = FindViewById<RelativeLayout>(Resource.Id.BtnScanMan2);

			BtnFine1 = FindViewById<RelativeLayout>(Resource.Id.BtnFine1);
			BtnFine2 = FindViewById<RelativeLayout>(Resource.Id.BtnFine2);

			FindViewById<ProgressBar>(Resource.Id.progressBar1).IndeterminateDrawable.SetColorFilter(Color.White, PorterDuff.Mode.Multiply);

			BtnScanFoto1.Click += async delegate
			{

				var options = new ZXing.Mobile.MobileBarcodeScanningOptions();
				options.PossibleFormats = new List<ZXing.BarcodeFormat>() {
					ZXing.BarcodeFormat.CODE_128,
					ZXing.BarcodeFormat.EAN_13
				};
				options.AutoRotate = false;
				options.TryHarder = true;

				scanOpen = true;

				scanner = new ZXing.Mobile.MobileBarcodeScanner();

				ZXing.Result result = null;

				new System.Threading.Thread(new System.Threading.ThreadStart(delegate
				{
					while (result == null)
					{
						Console.WriteLine("AF");
						scanner.AutoFocus();
						System.Threading.Thread.Sleep(2000);
					}
				})).Start();

				result = await scanner.Scan(options);

				if (result != null)
					HandleScanResult(result, 1);
				else
					scanOpen = false;
			};

			BtnScanFoto2.Click += async delegate
			{

				var options = new ZXing.Mobile.MobileBarcodeScanningOptions();
				options.PossibleFormats = new List<ZXing.BarcodeFormat>() {
					ZXing.BarcodeFormat.CODE_128,
					ZXing.BarcodeFormat.EAN_13
				};
				options.AutoRotate = false;
				options.TryHarder = true;

				scanOpen = true;

				scanner = new ZXing.Mobile.MobileBarcodeScanner();

				ZXing.Result result = null;

				new System.Threading.Thread(new System.Threading.ThreadStart(delegate
				{
					while (result == null)
					{
						Console.WriteLine("AF");
						scanner.AutoFocus();
						System.Threading.Thread.Sleep(2000);
					}
				})).Start();

				result = await scanner.Scan(options);

				if (result != null)
					HandleScanResult(result, 2);
				else
					scanOpen = false;
			};

			BtnScanMan1.Click += delegate
			{

				var inputDialog = new AlertDialog.Builder(this);
				EditText userInput = new EditText(this);
				userInput.Hint = "Codice prodotto";
				//SetEditTextStylings(userInput);
				userInput.InputType = Android.Text.InputTypes.TextFlagMultiLine;
				inputDialog.SetTitle("Articolo");
				inputDialog.SetMessage("Inserisci il codice prodotto.");
				inputDialog.SetView(userInput);
				inputDialog.SetCancelable(false);
				inputDialog.SetPositiveButton("Ok", (see, ess) =>
				{
					bool result = AddArticolo(userInput.Text.ToUpper(), 1);
					HideKeyboard(userInput);
					if (!result)
					{
						AlertDialog.Builder alert = new AlertDialog.Builder(this);
						alert.SetTitle("Errore");
						alert.SetMessage("Codice Errato o non presente");
						alert.SetPositiveButton("Continua", (senderAlert, args) =>
						{
						});
						alert.SetCancelable(false);
						alert.Show();
					}
				});

				inputDialog.SetNegativeButton("Annulla", (afk, kfa) =>
				{
					HideKeyboard(userInput);
				});

				inputDialog.Show();

				ShowKeyboard(userInput);

			};

			BtnScanMan2.Click += delegate
			{
				var inputDialog = new AlertDialog.Builder(this);
				EditText userInput = new EditText(this);
				userInput.Hint = "Codice prodotto";
				//SetEditTextStylings(userInput);
				userInput.InputType = Android.Text.InputTypes.TextFlagMultiLine;
				inputDialog.SetTitle("Articolo");
				inputDialog.SetMessage("Inserisci il codice prodotto.");
				inputDialog.SetView(userInput);
				inputDialog.SetCancelable(false);
				inputDialog.SetPositiveButton("Ok", (see, ess) =>
				{
					bool result = AddArticolo(userInput.Text.ToUpper(), 2);
					HideKeyboard(userInput);
					if (!result)
					{
						AlertDialog.Builder alert = new AlertDialog.Builder(this);
						alert.SetTitle("Errore");
						alert.SetMessage("Codice Errato o non presente");
						alert.SetPositiveButton("Continua", (senderAlert, args) =>
						{
						});
						alert.SetCancelable(false);
						alert.Show();
					}
				});

				inputDialog.SetNegativeButton("Annulla", (afk, kfa) =>
				{
					HideKeyboard(userInput);
				});

				inputDialog.Show();

				ShowKeyboard(userInput);
			};

			BtnFine1.Click += async delegate
			{

				var options = new ZXing.Mobile.MobileBarcodeScanningOptions();
				options.PossibleFormats = new List<ZXing.BarcodeFormat>() {
					ZXing.BarcodeFormat.CODE_128
				};
				options.AutoRotate = false;
				options.TryHarder = true;

				scanFineOpen = true;

				scanner = new ZXing.Mobile.MobileBarcodeScanner();

				ZXing.Result result = null;

				new System.Threading.Thread(new System.Threading.ThreadStart(delegate
				{
					while (result == null)
					{
						Console.WriteLine("AF");
						scanner.AutoFocus();
						System.Threading.Thread.Sleep(2000);
					}
				})).Start();

				result = await scanner.Scan(options);

				if (result != null)
					HandleScanResultFine(result, 1);
				else
					scanFineOpen = false;
			};

			BtnFine2.Click += async delegate
			{

				var options = new ZXing.Mobile.MobileBarcodeScanningOptions();
				options.PossibleFormats = new List<ZXing.BarcodeFormat>() {
					ZXing.BarcodeFormat.CODE_128
				};
				options.AutoRotate = false;
				options.TryHarder = true;

				scanFineOpen = true;

				scanner = new ZXing.Mobile.MobileBarcodeScanner();

				ZXing.Result result = null;

				new System.Threading.Thread(new System.Threading.ThreadStart(delegate
				{
					while (result == null)
					{
						Console.WriteLine("AF");
						scanner.AutoFocus();
						System.Threading.Thread.Sleep(2000);
					}
				})).Start();

				result = await scanner.Scan(options);

				if (result != null)
					HandleScanResultFine(result, 2);
				else
					scanFineOpen = false;
			};

			BtnCarrello1.Click += delegate
			{

				if (!isOne)
				{

					Riga1.SetBackgroundColor(CR1);
					Riga2.SetBackgroundColor(Color.White);

					Container1.Visibility = ViewStates.Visible;
					Container2.Visibility = ViewStates.Gone;

					isOne = true;
					isSecond = false;

				}

			};

			BtnCarrello2.Click += delegate
			{

				if (!isSecond)
				{

					Riga1.SetBackgroundColor(Color.White);
					Riga2.SetBackgroundColor(CR2);

					Container1.Visibility = ViewStates.Gone;
					Container2.Visibility = ViewStates.Visible;

					isOne = false;
					isSecond = true;

				}

			};

			timer = new Timer();
			timer.Interval = 10 * 60 * 1000;
			timer.Elapsed += (sender, e) =>
			{
				if (!scanFineOpen)
				{
					RunOnUiThread(() =>
					{

						if (scanOpen)
							scanner.Cancel();

						Load.Visibility = ViewStates.Visible;

						Console.WriteLine("TIMER");

						startDownload();

					});
				}

			};
			timer.Enabled = true;
			timer.Start();


			//********** SETTiNG FONT ***********

			Typeface osr = Typeface.CreateFromAsset(Assets, "fonts/OpenSansRegular.ttf");
			Typeface osb = Typeface.CreateFromAsset(Assets, "fonts/OpenSansBold.ttf");

			FindViewById<TextView>(Resource.Id.textView1).Typeface = osb;
			FindViewById<TextView>(Resource.Id.textView2).Typeface = osb;


			TotColli1.Typeface = osr;
			TotColli2.Typeface = osr;

			TotSpesa1.Typeface = osr;
			TotSpesa2.Typeface = osr;

			FindViewById<TextView>(Resource.Id.f1).Typeface = osb;
			FindViewById<TextView>(Resource.Id.f2).Typeface = osb;

			FindViewById<TextView>(Resource.Id.m1).Typeface = osb;
			FindViewById<TextView>(Resource.Id.m2).Typeface = osb;

			FindViewById<TextView>(Resource.Id.c1).Typeface = osb;
			FindViewById<TextView>(Resource.Id.c2).Typeface = osb;

			FindViewById<TextView>(Resource.Id.c1).Text = "CONCLUDI\nSPESA";
			FindViewById<TextView>(Resource.Id.c2).Text = "CONCLUDI\nSPESA";

			FindViewById<TextView>(Resource.Id.txt1).Typeface = osb;
			FindViewById<TextView>(Resource.Id.txt2).Typeface = osb;

			FindViewById<TextView>(Resource.Id.textView7).Typeface = osr;
			FindViewById<TextView>(Resource.Id.textView8).Typeface = osr;



		}


		void HandleScanResult(ZXing.Result result, int Indice)
		{

			scanOpen = false;

			string msg = "";

			if (result != null && !string.IsNullOrEmpty(result.Text))
			{
				string id = result.Text;
				this.RunOnUiThread(() => {

					bool result2 = AddArticolo(id, Indice);
					//HideKeyboard(userInput);
					if (!result2)
					{
						AlertDialog.Builder alert = new AlertDialog.Builder(this);
						alert.SetTitle("Errore");
						alert.SetMessage("Codice Errato o non presente");
						alert.SetPositiveButton("Continua", (senderAlert, args) =>
						{
						});
						alert.SetCancelable(false);
						alert.Show();
					}
					else
					{
						Toast.MakeText(this, "Prodotto inserito correttamente", ToastLength.Short).Show();
						ToneGenerator toneGen1 = new ToneGenerator(Stream.Music, 100);
						toneGen1.StartTone(Tone.CdmaAlertCallGuard, 1000);
					}
				});
			}
			else
			{

				this.RunOnUiThread(() => Toast.MakeText(this, msg, ToastLength.Short).Show());
			}


		}

		public bool AddArticolo(string id, int Indice)
		{
			Console.WriteLine("Add:"+id+" ed esiste:"+ArticoloDictionary.ContainsKey(id));
			if (ArticoloDictionary.ContainsKey(id))
			{
				
				if (Indice == 1)
				{

					bool GiaInserito = false;

					foreach (var a in LA1.items)
					{

						if (a.id == id)
						{
							GiaInserito = true;
							a.qnt++;
						}
					}

					ArticoloObject ao = ArticoloDictionary[id];

					if (!GiaInserito)
					{

						LA1.items.Add(new ArticoloCarrello(ao.id, 1, ao.prezzo, ao.titolo));

					}

					LA1.NotifyDataSetChanged();

					spesa1 += ao.prezzo;
					colli1++;

					TotColli1.Text = TXTCOLLI + " " + colli1;
					TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";

				}

				if (Indice == 2)
				{

					bool GiaInserito = false;

					foreach (var a in LA2.items)
					{

						if (a.id == id)
						{
							GiaInserito = true;
							a.qnt++;
						}
					}

					ArticoloObject ao = ArticoloDictionary[id];

					if (!GiaInserito)
					{

						LA2.items.Add(new ArticoloCarrello(ao.id, 1, ao.prezzo, ao.titolo));

					}

					LA2.NotifyDataSetChanged();

					spesa2 += ao.prezzo;
					colli2++;

					TotColli2.Text = TXTCOLLI + " " + colli2;
					TotSpesa2.Text = TXTSPESA + " " + spesa2.ToString("F") + "€";

				}

				return true;

			}
			else {
				return false;
			}

		}

		public void UpdateField(int indice, float prezzo, bool add)
		{

			if (indice == 1)
			{
				if (add)
				{
					spesa1 += prezzo;
					colli1++;
				}
				else {
					spesa1 -= prezzo;
					colli1--;
				}

				TotColli1.Text = TXTCOLLI + " " + colli1;
				TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";
			}

			if (indice == 2)
			{
				if (add)
				{
					spesa2 += prezzo;
					colli2++;
				}
				else {
					spesa2 -= prezzo;
					colli2--;
				}

				TotColli2.Text = TXTCOLLI + " " + colli2;
				TotSpesa2.Text = TXTSPESA + " " + spesa2.ToString("F") + "€";
			}

		}

		public void CheckRemove(ArticoloCarrello obj, int i)
		{


			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.SetTitle("Rimozione");
			alert.SetMessage("Stai rimuovendo " + obj.titolo);
			alert.SetPositiveButton("Continua", (senderAlert, args) =>
			{
				if (i == 1)
				{
					LA1.RemoveArticle(obj, true);
					UpdateField(i, obj.prezzo, false);
				}

				if (i == 2)
				{
					LA2.RemoveArticle(obj, true);
					UpdateField(i, obj.prezzo, false);
				}

			});

			alert.SetNegativeButton("Annulla", (senderAlert, args) =>
			{
				if (i == 1)
					LA1.RemoveArticle(obj, false);

				if (i == 2)
					LA2.RemoveArticle(obj, false);
			});

			alert.SetCancelable(false);

			alert.Show();

		}

		public void ShowKeyboard(EditText userInput)
		{
			userInput.RequestFocus();
			InputMethodManager imm = (InputMethodManager)this.GetSystemService(Context.InputMethodService);
			imm.ToggleSoftInput(ShowFlags.Forced, 0);
		}

		public void HideKeyboard(EditText userInput)
		{
			InputMethodManager imm = (InputMethodManager)this.GetSystemService(Context.InputMethodService);
			imm.HideSoftInputFromWindow(userInput.WindowToken, 0);
		}

		public void startDownload()
		{

			/* v1
			//PRODUZIONE
			var client = new RestClient("http://2.228.89.216");
			//DEVELOP
			//var client = new RestClient("http://test.netwintec.com");

			var request = new RestRequest("/prestospesa-backend/read.php", Method.GET);

			request.AddHeader("access-token", "SCOj4qU5JB4ysjUKG1lDgR04hID4WU");

			client.ExecuteAsync(request, (s, e) =>
			{


				//Console.WriteLine(s.StatusCode + "\nContent:\n" + s.Content);

				if (s.StatusCode == HttpStatusCode.OK)
				{

					RunOnUiThread(() =>
					{

						string csv = s.Content;

						//Console.WriteLine(1);

						string[] Lines = csv.Split('\n');

						//Console.WriteLine("2|" + Lines.Length);

						for (int i = 0; i < Lines.Length - 1; i++)
						{
							try
							{
								string lines = Lines[i];
								//Console.WriteLine(lines);
								string[] Value = lines.Split(';');

								//Console.WriteLine(Value[0] + "|" + Value[1] + "|" + Value[2]);

								string Id = Value[0];
								string Tit = Value[1];
								string PrApp = Value[2];

								float Pr = float.Parse(PrApp);//PrApp.Replace(',', '.'));

								if (Id != ".")
								{
									if (ArticoloDictionary.ContainsKey(Id))
										ArticoloDictionary[Id] = new ArticoloObject(Id, Pr, Tit);
									else
										ArticoloDictionary.Add(Id, new ArticoloObject(Id, Pr, Tit));
								}

							}
							catch (Exception ee)
							{

							}
						}

						//Console.WriteLine(3);

						spesa1 = 0;
						spesa2 = 0;

						foreach (var a in LA1.items)
						{
							a.prezzo = ArticoloDictionary[a.id].prezzo;

							spesa1 += (a.prezzo * a.qnt);

						}

						foreach (var b in LA2.items)
						{
							b.prezzo = ArticoloDictionary[b.id].prezzo;

							spesa2 += (b.prezzo * b.qnt);
						}

						TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";
						TotSpesa2.Text = TXTSPESA + " " + spesa2.ToString("F") + "€";

						LA1.NotifyDataSetChanged();
						LA2.NotifyDataSetChanged();

						Load.Visibility = ViewStates.Gone;

					});
				}
				else
				{
				}

			});

*/

			/** V2 **/

			MainActivity.Instance.timeoutPage = true;

			try
			{

				var webclient = new MyWebClientPage();

				webclient.DownloadDataCompleted += (s, e) =>
				{

					RunOnUiThread(() =>
					{
						MainActivity.Instance.timeoutPage = false;
					});

					if (e.Cancelled)
					{
						Console.WriteLine("CANCELLED");
					}
					if (e.Error != null)
					{
						Console.WriteLine("ERROR" + e.Error.Message);

						if (e.Error.Message.Contains("401"))
						{
							RunOnUiThread(() =>
							{
								spesa1 = 0;
								colli1 = 0;
								LA1.items.Clear();

								TotColli1.Text = TXTCOLLI + " " + colli1;
								TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";

								LA1.NotifyDataSetChanged();

								spesa2 = 0;
								colli2 = 0;
								LA2.items.Clear();

								TotColli2.Text = TXTCOLLI + " " + colli2;
								TotSpesa2.Text = TXTSPESA + " " + spesa2.ToString("F") + "€";

								LA2.NotifyDataSetChanged();

								Load.Visibility = ViewStates.Gone;
								//Errore(1);
							});
						}
						else {
							RunOnUiThread(() =>
							{
								Load.Visibility = ViewStates.Gone;
								//Errore(2);
							});
						}

					}
					if (e.Error == null && !e.Cancelled)
					{
						RunOnUiThread(() =>
						{
							var bytes = e.Result;
							string result = System.Text.Encoding.UTF8.GetString(bytes);
							//Console.WriteLine("Result" + result);

							string csv = result;

							Console.WriteLine(1);

							string[] Lines = csv.Split('\n');

							//Console.WriteLine("2|" + Lines.Length);

							for (int i = 0; i < Lines.Length - 1; i++)
							{
								try
								{
									string lines = Lines[i];
									//Console.WriteLine(lines);
									string[] Value = lines.Split(';');

									//Console.WriteLine(Value[0] + "|" + Value[1] + "|" + Value[2]);

									string Id = Value[0];
									string Tit = Value[1];
									string PrApp = Value[2];

									float Pr = float.Parse(PrApp);//PrApp.Replace(',', '.'));

									if (Id != ".")
									{
										if (ArticoloDictionary.ContainsKey(Id))
											ArticoloDictionary[Id] = new ArticoloObject(Id, Pr, Tit);
										else
											ArticoloDictionary.Add(Id, new ArticoloObject(Id, Pr, Tit));
									}

								}
								catch (Exception ee)
								{

								}
							}

							//Console.WriteLine(3);

							spesa1 = 0;

							foreach (var a in LA1.items)
							{
								a.prezzo = ArticoloDictionary[a.id].prezzo;

								spesa1 += (a.prezzo * a.qnt);

							}


							TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";

							LA1.NotifyDataSetChanged();

							Load.Visibility = ViewStates.Gone;

						});

					}
				};

				// DEVELOPMENT
				//var url = new System.Uri("http://test.netwintec.com/prestospesa-backend/read.php");
				//PRODUZIONE
				var url = new System.Uri("http://2.228.89.216/prestospesa-backend/read.php");


				var header = new WebHeaderCollection();
				header.Add("access-token", "SCOj4qU5JB4ysjUKG1lDgR04hID4WU");
				webclient.Headers = header;


				webclient.DownloadDataAsync(url);

			}
			catch (Exception ee) { }

		}


		void HandleScanResultFine(ZXing.Result result, int Indice)
		{

			scanOpen = false;

			string msg = "";

			if (result != null && !string.IsNullOrEmpty(result.Text))
			{
				string txt = result.Text;
				this.RunOnUiThread(() =>{

					Toast.MakeText(this, "Operazione in corso\nAttendere...", ToastLength.Short).Show();
					ChiudiCarrello(txt, Indice);
				});
			}
			else
			{

				this.RunOnUiThread(() => Toast.MakeText(this, msg, ToastLength.Short).Show());
			}


		}


		public void ChiudiCarrello(string txt, int indice)
		{

			string carrello = "";

			if (indice == 1)
			{
				foreach (var a in LA1.items)
				{

					if (a.id.Length == 12)
						carrello += a.id + " ";
					else
						carrello += a.id;

					if (a.qnt > 0 && a.qnt < 10)
						carrello += "000" + a.qnt;

					if (a.qnt >= 10 && a.qnt < 100)
						carrello += "00" + a.qnt;

					if (a.qnt >= 100 && a.qnt < 1000)
						carrello += "0" + a.qnt;

					if (a.qnt >= 1000 && a.qnt < 10000)
						carrello += a.qnt;

					carrello += "\r\n";
				}
			}

			if (indice == 2)
			{
				foreach (var a in LA2.items)
				{

					if (a.id.Length == 12)
						carrello += a.id + " ";
					else
						carrello += a.id;

					if (a.qnt > 0 && a.qnt < 10)
						carrello += "000" + a.qnt;

					if (a.qnt >= 10 && a.qnt < 100)
						carrello += "00" + a.qnt;

					if (a.qnt >= 100 && a.qnt < 1000)
						carrello += "0" + a.qnt;

					if (a.qnt >= 1000 && a.qnt < 10000)
						carrello += a.qnt;

					carrello += "\r\n";
				}
			}

			/* v1
			//PRODUZIONE
			var client = new RestClient("http://2.228.89.216");
			//DEVELOP
			//var client = new RestClient("http://test.netwintec.com");

			var request = new RestRequest("/prestospesa-backend/write.php", Method.POST);

			request.AddHeader("access-token", "SCOj4qU5JB4ysjUKG1lDgR04hID4WU");

			request.AddParameter("codice_cassa", txt);
			request.AddParameter("spesa", carrello);

			client.ExecuteAsync(request, (s, e) =>
			{


				Console.WriteLine("CHIUSURA:" + s.StatusCode + "\nContent:\n" + s.Content);

				if (s.StatusCode == HttpStatusCode.OK)
				{

					RunOnUiThread(() =>
					{

						if (indice == 1)
						{
							ViewFine1.Visibility = ViewStates.Visible;
						}

						if (indice == 2)
						{
							ViewFine2.Visibility = ViewStates.Visible;
						}

					});
				}
				else
				{
					RunOnUiThread(() =>
					{

						AlertDialog.Builder alert = new AlertDialog.Builder(this);
						alert.SetTitle("Errore");
						alert.SetMessage("Errore durante l'operazione di chiusura riprovare.");
						alert.SetPositiveButton("OK", (senderAlert, args) =>
						{
						});
						alert.SetCancelable(false);
						alert.Show();

					});
				}

			});

*/

			// v2 

			MainActivity.Instance.timeoutChiusura = true;

			try
			{

				var webclient = new MyWebClientChiusura();

				webclient.UploadValuesCompleted += (s, e) =>
				{

					RunOnUiThread(() =>
					{
						MainActivity.Instance.timeoutChiusura = false;
					});

					if (e.Cancelled)
					{
						Console.WriteLine("CANCELLED");
					}
					if (e.Error != null)
					{
						Console.WriteLine("ERROR" + e.Error.Message);

						if (e.Error.Message.Contains("401"))
						{
							RunOnUiThread(() =>
							{
								AlertDialog.Builder alert = new AlertDialog.Builder(this);
								alert.SetTitle("Errore");
								alert.SetMessage("Errore durante l'operazione di chiusura riprovare.");
								alert.SetPositiveButton("OK", (senderAlert, args) =>
								{
								});
								alert.SetCancelable(false);
								alert.Show();
							});
						}
						else
						{
							RunOnUiThread(() =>
							{
								AlertDialog.Builder alert = new AlertDialog.Builder(this);
								alert.SetTitle("Errore");
								alert.SetMessage("Errore durante l'operazione di chiusura riprovare.");
								alert.SetPositiveButton("OK", (senderAlert, args) =>
								{
								});
								alert.SetCancelable(false);
								alert.Show();
							});
						}

					}
					if (e.Error == null && !e.Cancelled)
					{
						RunOnUiThread(() =>
						{
							if (indice == 1)
							{
								ViewFine1.Visibility = ViewStates.Visible;
							}

							if (indice == 2)
							{
								ViewFine2.Visibility = ViewStates.Visible;
							}


						});

					}
				};

				// DEVELOPMENT
				//var url = new System.Uri("http://test.netwintec.com/prestospesa-backend/write.php");
				//PRODUZIONE
				var url = new System.Uri("http://2.228.89.216/prestospesa-backend/write.php");

				var header = new WebHeaderCollection();
				header.Add("access-token", "SCOj4qU5JB4ysjUKG1lDgR04hID4WU");
				webclient.Headers = header;

				//request.AddParameter("codice_cassa", txt);
				//request.AddParameter("spesa", carrello);

				//string dataString = @"{'codice_cassa':'" + txt + "','spesa':'" + carrello + "'}";
				//byte[] dataBytes = Encoding.UTF8.GetBytes(dataString);

				NameValueCollection parameter = new NameValueCollection();
				parameter.Add("codice_cassa", txt);
				parameter.Add("spesa", carrello);

				webclient.UploadValuesAsync(url, "POST", parameter);

			}
			catch (Exception ee) { }
		}

		public override void OnBackPressed()
		{
			//base.OnBackPressed();

			Exit();

		}

		public void Exit()
		{
			//Alert vuoi sloggarti?
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.SetCancelable(false);
			alert.SetTitle("Sicuro di voler chiudere l'applicazione?");
			alert.SetPositiveButton("Si", (senderAlert, args) =>
			{

				//System.Environment.Exit(0);
				Android.OS.Process.KillProcess(Android.OS.Process.MyPid());

			});
			alert.SetNegativeButton("Annulla", (senderAlert, args) =>
			{
				//volendo fa qualcosa
			});
			//fa partire l'alert su di un trhead
			RunOnUiThread(() =>
			{
				alert.Show();
			});
		}
	}
}



