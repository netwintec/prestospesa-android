﻿using System;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Android.Net;
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Content;
using Android.Webkit;
using ZXing.Mobile;
using RestSharp;
using Android.Content.PM;
using Android.Graphics;
using Android.Content.Res;
using System.Globalization;

namespace PrestoSpesa
{
	[Activity(Label = "Presto Spesa", MainLauncher = false, ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : Activity
	{
		LinearLayout Scelta;
		RelativeLayout Download;
		ProgressBar pb;
		TextView pbP;

		public bool timeout = true;
		public bool timeoutPage = true;
		public bool timeoutChiusura = true;

		public int nCarrelli = 0;

		public Dictionary<string, ArticoloObject> ArticoloDictionary = new Dictionary<string, ArticoloObject>();

		public static MainActivity Instance { private set; get; }

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			ActionBar.Hide();

			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.Main);

			MobileBarcodeScanner.Initialize(Application);

			MainActivity.Instance = this;

			Scelta = FindViewById<LinearLayout>(Resource.Id.SceltaCarrelli);
			Download = FindViewById<RelativeLayout>(Resource.Id.Download);

			Scelta.Visibility = ViewStates.Visible;
			Download.Visibility = ViewStates.Gone;

			RelativeLayout BTNCarrelo1 = FindViewById<RelativeLayout>(Resource.Id.BtnCarrello1);
			RelativeLayout BTNCarrelo2 = FindViewById<RelativeLayout>(Resource.Id.BtnCarrello2);
			RelativeLayout BTNCarrelo3 = FindViewById<RelativeLayout>(Resource.Id.BtnCarrello3);

			pb = FindViewById<ProgressBar>(Resource.Id.progress);
			//pb.IndeterminateDrawable.SetColorFilter(Color.Rgb(57, 174, 107), PorterDuff.Mode.Clear);

			pbP = FindViewById<TextView>(Resource.Id.ProgressPerc);

			BTNCarrelo1.Click += delegate
			{

				nCarrelli = 1;

				Console.WriteLine("N° Carrelli:" + nCarrelli);

				Scelta.Visibility = Android.Views.ViewStates.Gone;
				Download.Visibility = Android.Views.ViewStates.Visible;

				startDownload();

			};

			BTNCarrelo2.Click += delegate
			{

				nCarrelli = 2;

				Console.WriteLine("N° Carrelli:" + nCarrelli);

				Scelta.Visibility = Android.Views.ViewStates.Gone;
				Download.Visibility = Android.Views.ViewStates.Visible;

				startDownload();

			};

			BTNCarrelo3.Click += delegate
			{

				nCarrelli = 3;

				Console.WriteLine("N° Carrelli:" + nCarrelli);

				Scelta.Visibility = Android.Views.ViewStates.Gone;
				Download.Visibility = Android.Views.ViewStates.Visible;

				startDownload();

			};



			Typeface oscb = Typeface.CreateFromAsset(Assets, "fonts/OpenSansCondBold.ttf");
			FindViewById<TextView>(Resource.Id.textView5).Typeface = oscb;
			FindViewById<TextView>(Resource.Id.textView6).Typeface = oscb;

			pbP.Typeface = oscb;


			/* TEST *

			bool errore = false;
			string content="";
			try
			{
				AssetManager assets = this.Assets;
				using (StreamReader sr = new StreamReader(assets.Open("IN.TXT")))
				{
					content = sr.ReadToEnd();
				}

			}
			catch (IOException ex)
			{
				Console.WriteLine("ge");
				errore = true;
			}
		
			string result = content;
			//Console.WriteLine("Result" + result);

			string csv = result;

			Console.WriteLine(1);

			string[] Lines = csv.Split('\n');

			Console.WriteLine("2|" + Lines.Length);

			for (int i = 0; i < Lines.Length - 1; i++)
			{
				try
				{
					string lines = Lines[i];
					//Console.WriteLine(lines);
					string[] Value = lines.Split(';');

					Console.WriteLine(Value[0] + "|" + Value[1] + "|" + Value[2]);

					string Id = Value[0];
					string Tit = Value[1];
					string PrApp = Value[2];

					float Pr = float.Parse(PrApp);//PrApp.Replace(',', '.'));

					if (Id != ".")
						ArticoloDictionary.Add(Id, new ArticoloObject(Id, Pr, Tit));
				}
				catch (Exception ee)
				{
					errore = true;

				}
			}

			if (errore)
			{
				Errore(2);
			}
			else
			{
				Console.WriteLine(3);


				var intent = new Intent(this, typeof(HomePageOne));
				StartActivity(intent);


			}

			/***************/
	

		}

		public void startDownload()
		{


			/** v1 **
			//PRODUZIONE
			var client = new RestClient("http://2.228.89.216");
			//DEVELOP
			//var client = new RestClient("http://test.netwintec.com");

			var request = new RestRequest("/prestospesa-backend/read.php", Method.GET);

			request.AddHeader("access-token", "SCOj4qU5JB4ysjUKG1lDgR04hID4WU");

			client.ExecuteAsync(request, (s, e) =>
			{


				Console.WriteLine(s.StatusCode + "\nContent:\n" + s.Content);

				if (s.StatusCode == HttpStatusCode.OK)
				{

					RunOnUiThread(() =>
					{

						string csv = s.Content;

						Console.WriteLine(1);

						string[] Lines = csv.Split('\n');

						Console.WriteLine("2|" + Lines.Length);

						for (int i = 0; i < Lines.Length - 1; i++)
						{
							try
							{
								string lines = Lines[i];
								//Console.WriteLine(lines);
								string[] Value = lines.Split(';');

								Console.WriteLine(Value[0] + "|" + Value[1] + "|" + Value[2]);

								string Id = Value[0];
								string Tit = Value[1];
								string PrApp = Value[2];

								float Pr = float.Parse(PrApp);//PrApp.Replace(',', '.'));

								if (Id != ".")
									ArticoloDictionary.Add(Id, new ArticoloObject(Id, Pr, Tit));
							}
							catch (Exception ee)
							{

								Errore(2);

							}
						}

						Console.WriteLine(3);

						if (nCarrelli == 1)
						{
							var intent = new Intent(this, typeof(HomePageOne));
							StartActivity(intent);
						}

						if (nCarrelli == 2)
						{
							var intent = new Intent(this, typeof(HomePageTwo));
							StartActivity(intent);
						}

						if (nCarrelli == 3)
						{
							var intent = new Intent(this, typeof(HomePageThree));
							StartActivity(intent);
						}

					});

				}
				if (s.StatusCode == HttpStatusCode.Unauthorized)
				{ 
					RunOnUiThread(() =>
					{
						Errore(1);
					});
				}
				if (s.StatusCode != HttpStatusCode.Unauthorized && s.StatusCode != HttpStatusCode.OK)
				{
					RunOnUiThread(() =>
					{
						Errore(2);
					});
				}

			});

			*/

			/** V2 **/

			timeout = true;

			try
			{
				
				var webclient = new MyWebClient();

				webclient.DownloadDataCompleted += (s, e) => {

					RunOnUiThread(() =>
					{
						timeout = false;
					});
					              
					if (e.Cancelled) {
						Console.WriteLine("CANCELLED");
					}
					if (e.Error != null)
					{
						Console.WriteLine("ERROR"+e.Error.Message);

						if (e.Error.Message.Contains("401"))
						{
							RunOnUiThread(() =>
							{
								Errore(1);
							});
						}
						else {
							RunOnUiThread(() =>
							{
								Errore(2);
							});
						}

					}
					if (e.Error == null && !e.Cancelled)
					{
						RunOnUiThread(() =>
						{	
							var bytes = e.Result;
							string result = System.Text.Encoding.UTF8.GetString(bytes);
							//Console.WriteLine("Result" + result);

							string csv = result;

							Console.WriteLine(1);

							string[] Lines = csv.Split('\n');

							Console.WriteLine("2|" + Lines.Length);

							bool errore = false;

							for (int i = 0; i < Lines.Length - 1; i++)
							{
								try
								{
									string lines = Lines[i];
									//Console.WriteLine(lines);
									string[] Value = lines.Split(';');

									Console.WriteLine(Value[0] + "|" + Value[1] + "|" + Value[2]);

									string Id = Value[0];
									string Tit = Value[1];
									string PrApp = Value[2];

									var culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
									culture.NumberFormat.NumberDecimalSeparator = ",";
							
									float Pr = float.Parse(PrApp,culture);//PrApp.Replace(',', '.'));

									if (Id != ".")
										ArticoloDictionary.Add(Id, new ArticoloObject(Id, Pr, Tit));
								}
								catch (Exception ee)
								{
									errore = true;

								}
							}

							if (errore)
							{
								Errore(2);
							}
							else
							{
								Console.WriteLine(3);

								if (nCarrelli == 1)
								{
									var intent = new Intent(this, typeof(HomePageOne));
									StartActivity(intent);
								}

								if (nCarrelli == 2)
								{
									var intent = new Intent(this, typeof(HomePageTwo));
									StartActivity(intent);
								}

								if (nCarrelli == 3)
								{
									var intent = new Intent(this, typeof(HomePageThree));
									StartActivity(intent);
								}
							}
						});

					}
				};

				webclient.DownloadProgressChanged += (sender, e) => {

					var bR = e.BytesReceived / 1024;
					var trtR = e.TotalBytesToReceive / 1024;
					Console.WriteLine(bR + "/" + trtR +" kB");
					Console.WriteLine(e.ProgressPercentage+ "%");

					RunOnUiThread(() =>
					{
						pb.Progress = e.ProgressPercentage;
						pbP.Text = e.ProgressPercentage + "%";
						//pbD.Text = bR + "/" + trtR + " kB";
							
					});
				};

				// DEVELOPMENT
				//var url = new System.Uri("http://test.netwintec.com/prestospesa-backend/read.php");
				//PRODUZIONE
				var url = new System.Uri("http://app.gruppobonechi.it/prestospesa-backend/read.php");


				var header = new WebHeaderCollection();
				header.Add("access-token", "SCOj4qU5JB4ysjUKG1lDgR04hID4WU");
				webclient.Headers = header;


				webclient.DownloadDataAsync(url);

			}
			catch (Exception ee) { }






			/**/

		}

		public void Errore(int i) {

			if (i == 1) { 
				AlertDialog.Builder alert = new AlertDialog.Builder(this);
				alert.SetTitle("Errore");
				alert.SetMessage("Il mercato delle opportunità è chiuso impossibile scaricare prodotti");
				alert.SetPositiveButton("OK", (senderAlert, args) =>
				{
					Scelta.Visibility = ViewStates.Visible;
					Download.Visibility = ViewStates.Gone;
				});
				alert.SetCancelable(false);
				alert.Show();
			}
			if (i == 2)
			{
				AlertDialog.Builder alert = new AlertDialog.Builder(this);
				alert.SetTitle("Errore");
				alert.SetMessage("Caricamento dati non riuscito riprovare");
				alert.SetPositiveButton("OK", (senderAlert, args) =>
				{
					Scelta.Visibility = ViewStates.Visible;
					Download.Visibility = ViewStates.Gone;
				});
				alert.SetCancelable(false);
				alert.Show();
			}
		}
	
	}

}


