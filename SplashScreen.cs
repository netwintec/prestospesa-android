﻿using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Content;
using System.Threading;
using System.Threading.Tasks;
using Android.Widget;
using Android.Graphics;

namespace PrestoSpesa
{
	[Activity( MainLauncher = true, NoHistory = true, Theme = "@style/Theme.Splash2", ScreenOrientation = ScreenOrientation.Portrait)]
	public class SplashScreen : Activity
	{
		bool notification;
		string name,conv_id;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			Typeface oscb = Typeface.CreateFromAsset(Assets, "fonts/OpenSansCondBold.ttf");

			SetContentView (Resource.Layout.SplashScreen);
			FindViewById<TextView>(Resource.Id.textView1).Text = "BENVENUTI\nAL MERCATO\nDELLE\nOPPORTUNITÀ";
			FindViewById<TextView>(Resource.Id.textView1).Typeface = oscb;
			                                             
			ThreadPool.QueueUserWorkItem(o=> LoadActivity ());

		}
		public void LoadActivity(){
			Thread.Sleep (2000);
			RunOnUiThread (() => {
				var intent = new Intent(this, typeof(MainActivity));

				StartActivity(intent);
				Finish();
			});
		}
	}
}

