﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace PrestoSpesa
{
	public class MyWebClientPage : WebClient
	{
		protected override WebRequest GetWebRequest(System.Uri address)
		{

			WebRequest w = base.GetWebRequest(address);
			//w.Timeout = 3 * 1000;

			new System.Threading.Thread(new System.Threading.ThreadStart(() =>
			{
				try
				{
					System.Threading.Thread.Sleep(10 * 1000);
					MainActivity.Instance.RunOnUiThread(() =>
					{
						if (MainActivity.Instance.timeoutPage)
						{
							w.Abort();
						}
					});
				}
				catch (Exception e)
				{
					MainActivity.Instance.RunOnUiThread(() =>
					{
						Console.WriteLine("Error getting the audio file\n" + e.StackTrace + "\n" + e.Message);
					});
				}
			})).Start();

			return w;
		}
	}
}
